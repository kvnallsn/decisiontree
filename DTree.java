/**
 * File: DTree.java
 *
 * Represents a decision tree using the ID3 Algorithm
 *
 * @author: Kevin Allison
 */

import java.io.*;
import java.util.*;

class Attribute {
	public String name;
	public Map<String, String> opts;
	int position;

	public Attribute(String inputLine, int position) {

		this.position = position;
		this.opts = new LinkedHashMap<String, String>();

		// Parse out the name of the attribute
		int name_split_idx = inputLine.indexOf(':');
		this.name = inputLine.substring(0, name_split_idx);
		inputLine = inputLine.substring(name_split_idx+1);

		String[] classes = inputLine.split(",");
		for (String s : classes) {
			// Split on equals sign
			String[] desig = s.split("=");
			this.opts.put(desig[0], desig[1]);
		}
	}

	public Attribute(String value) {
		this.name = value;
		this.opts = null;
		position = -1;
	}
}

class AttributeInfo {
	public Attribute attr;
	public double info_gain;
	public Map<String, Double> entropy_map;

	public AttributeInfo(Attribute attr, double ig, Map<String, Double> entropy_map) {
		this.attr = attr;
		this.info_gain = ig;
		this.entropy_map = entropy_map;
	}
}

class ClassInfo {
	public String name;
	public int value;

	public ClassInfo(String name, int value) {
		this.name = name;
		this.value = value;
	}
}

class DNode {
    Map<String, DNode> children;
    Attribute attr;  // The attribute at this node

    public DNode(Attribute attr, Map<String, DNode> children ) {
        this.attr = attr;
        this.children = children;
    }
}

public class DTree {

	public static double LOG2 = Math.log10(2);
    public static PrintWriter out;

	// Class Variables
	Map<String, ClassInfo> class_map;	// Map the classes to their expanded names
	DNode root;

	/**
	* Constructor for a Decision Tree
	*
    * @param class_map  The class map to use
	* @param attrs		The list of attributes to use
	* @param case_list	The list of cases to start with
	*/
	public DTree(Map<String, ClassInfo> class_map, List<Attribute> attrs, List<String[]> case_list) {

		this.class_map = class_map;
        root = buildTree(attrs, case_list, 0);
	}


	/**
	* Compute the entropy for a given value of an attribute
	*
	* @param attr 		The current attribute
	* @param value		The value of the attribute to look at
	* @param case_list	The case list to search through
	*
	* @return 			The weighted entropy value, between 0 and 1
	*/
	private double computeEntropy(Attribute attr, String value, List<String[]> case_list) {

		double num_false = 0;
		double total = 0;
		//System.out.print("\t\t");
		for (String[] case_info : case_list) {
			if (case_info[attr.position+1].equals(value)) {
				//System.out.print("|");
				num_false += class_map.get(case_info[0]).value;
				++total;
			}
		}
		//System.out.print("\t" + num_false + " / " + total);

		double num_true = total - num_false;
		double false_entropy =  -((num_false / total) * (Math.log10( num_false / total) / LOG2));
		double true_entropy =  (num_true / total) * (Math.log10( num_true / total) / LOG2);

		double entropy = (num_true == 0 || num_false == 0) ? 0.0 : false_entropy - true_entropy;
		double weighted_entropy = entropy * (total / (double)case_list.size());

		//System.out.print("\t" + entropy + "\t" + weighted_entropy + "\n");
		return weighted_entropy;
	}

	/** 
	* Compute the inforomation gain for an attribute
	*
	* @param attr		The attribute to compute the info gain for
	* @param case_list	The case list to search through
	*
	* @return 			The information gain (1 - weighted entropy) for this attribute
	*/
	private double computeInfoGain(Attribute attr, List<String[]> case_list) {
	
		double weighted_entropy = 0.0;
		// Look through cases and pick ones that match the given attribute
		//System.out.println("Attribute: " + attr.name);
		for (String value : attr.opts.keySet()) {
			//System.out.println("\t" + attr.opts.get(value));
			weighted_entropy += computeEntropy(attr, value, case_list);
		}
		//System.out.println("\tInfo Gain: " + (1 - weighted_entropy));
		return (1 - weighted_entropy);
	}

	/**
	* Pick return the best attribute to use at the given level
	*
	* @param attr_list	The list of attributes to pick from
	* @param case_list	The case list to search through
	*
	* @return 			The best attribute to use
	*/
	private AttributeInfo findBestAttribute(List<Attribute> attr_list, List<String[]> case_list) {
		double highest_info_gain = 0.0;
		Attribute best_attr = null;
		Map<String, Double> best_entropy_map = new LinkedHashMap<String, Double>();
		for (Attribute attr : attr_list) {
			Map<String, Double> entropy_map = new LinkedHashMap<String, Double>();
			double weighted_entropy = 0.0;
			for (String value : attr.opts.keySet()) {
				double entropy = computeEntropy(attr, value, case_list);
				entropy_map.put(value, entropy);
				weighted_entropy += entropy;
			}
			double info_gain = 1 - weighted_entropy;
			if (info_gain > highest_info_gain) {
				highest_info_gain = info_gain;
				best_attr = attr;
				best_entropy_map = entropy_map;
			}
		}

		return new AttributeInfo(best_attr, highest_info_gain, best_entropy_map);
	}

    /**
    * Build the decision tree
    *
	* @param attr_list	The list of attributes to pick from
	* @param case_list	The case list to search through
    *
    * @return           The root node of the new tree create
    */
    private DNode buildTree(List<Attribute> attr_list, List<String[]> case_list, int lvl) {

        // Children structure
        Map<String, DNode> children = new LinkedHashMap<String, DNode>();

        // Find the best attribute in the given list
        AttributeInfo best = findBestAttribute(attr_list, case_list);
		out.println("Level " + lvl + " Attribute: " + best.attr.name);

        for (String key : best.attr.opts.keySet()) {
			List<String[]> new_case_list = new ArrayList<String[]>();
			for (String[] case_ex : case_list) {
				if (case_ex[best.attr.position+1].equals(key)) {
					new_case_list.add(case_ex);
				}
			}

			if (new_case_list.size() == 0) {
				// If no cases, then it is undetermined
				out.print("At level " + (lvl+1) + ", ");
				out.print(key + "=" + best.attr.opts.get(key));
				out.println(", decision: undetermined");
				children.put(key, new DNode(new Attribute("undetermined"), null));
			} else if (best.entropy_map.get(key) == 0.0) {
				// If the entropy for a given option is zero, set it as leaf node
				out.print("At level " + (lvl+1) + ", ");
				out.print(key + "=" + best.attr.opts.get(key));
				out.print(", decision: " + new_case_list.get(0)[0] + "=");
				out.println(class_map.get(new_case_list.get(0)[0]).name);
				children.put(key, new DNode(new Attribute(new_case_list.get(0)[0]), null));
			} else {
				// Otherwise keep building the tree
				out.println("Split tree on " + key + "=" + best.attr.opts.get(key));
				List<Attribute> new_attr_list = new ArrayList<Attribute>();
				new_attr_list.addAll(attr_list);
				new_attr_list.remove(best.attr);
				children.put(key, buildTree(new_attr_list, new_case_list, lvl+1));
			}

        }

        return new DNode(best.attr, children);
    }

	/**
	* Traverse the tree in a depth first pattern
	*
	* @param root		The start of the tree to traverse
	*/
	public void traverse(DNode node, int level) {
		System.out.println(level + ") Node: " + node.attr.name);	
		if (node.children != null) {
			for (String s : node.children.keySet()) {
				traverse(node.children.get(s), level+1);
			}
		}
	}

	/**
	* Test the given case and see if it is classifed properly
	*
	* @param t_case 			The test case array
	*
	* @return True if the tree decides it properly, false otherwise
	*/
	private boolean testCase(DNode node, String[] t_case) {
		// Get the node's attribute
		if (node.children != null) {
			return testCase(node.children.get(t_case[node.attr.position+1]), t_case);
		}

		return node.attr.name.equals(t_case[0]);
	}

	/**
	* Test a list of cases and see how many pass/fail
	*
	* @param test_list		A String[] of tests to run
	*
	* @return 				Number of tests that passed
	*/
	public int test(List<String[]> test_list) {
		int num_passed = 0;
		for (String[] test_case : test_list) {
			if (testCase(root, test_case)) num_passed++;
		}

		return num_passed;
	}

	public static void main(String[] args) {

		if (args.length != 3) {
			System.out.println("Usage: java DTree inputfile outputfile percent");
			System.exit(0);
		}

		double percent_train = -1.0;

		try {
			percent_train = Double.parseDouble(args[2]);
			if (percent_train < 1.0 || percent_train > 100.0) {
				throw new NumberFormatException();
			} 

			File inputFile = new File(args[0]);
			if (!inputFile.exists()) {
				System.out.println("File not found: " + inputFile.getName());
				System.exit(0);
			}
			
            if (args[1].equals("-")) {
                out = new PrintWriter(System.out, true);
            } else {
                out = new PrintWriter(new FileOutputStream(new File(args[1])), true);
            }

			BufferedReader inputReader = new BufferedReader(new FileReader(inputFile));

			int num_attr = Integer.parseInt(inputReader.readLine());
			int num_examples = Integer.parseInt(inputReader.readLine());

			// Read in the classes
			String classes = inputReader.readLine();
			classes = classes.substring(classes.indexOf(':')+1);
			Map<String, ClassInfo> class_map = new LinkedHashMap<String, ClassInfo>();

			String[] classes_array = classes.split(",");
			for (int i = 0; i < classes_array.length; i++) {
				String s = classes_array[i];
				// Split on equals sign
				String[] desig = s.split("=");
				class_map.put(desig[0], new ClassInfo(desig[1], i));
			}
			class_map.put("--", new ClassInfo("undetermined", -1));

			// Read in blank line
			inputReader.readLine();

			String line;
			List<Attribute> attributes = new ArrayList<Attribute>();
			for (int i = 0; i < num_attr; i++) {
				attributes.add(new Attribute(inputReader.readLine(), i));
			}

			double num_train = (percent_train / 100.0) * num_examples;
			List<String[]> training_cases = new ArrayList<String[]>();
			for (int i = 0; i < num_train && ((line = inputReader.readLine()) != null); i++) {
				training_cases.add(line.split(","));
			}

			List<String[]> test_cases = new ArrayList<String[]>();
			if (percent_train == 100) {
				test_cases = training_cases;
			} else {
				while ((line = inputReader.readLine()) != null) {
					test_cases.add(line.split(","));
				}
			}

			out.println();
			out.println("The Decision Tree Built With " + training_cases.size() + " Training Samples:");
			out.println();

			DTree dTree = new DTree(class_map, attributes, training_cases);
			int num_passed = dTree.test(test_cases);

			out.println();
			out.println("Classification Results On " + test_cases.size() + " Test Samples:");
			out.println();
			out.println("Number of correct classifications: " + num_passed);
			out.println("Number of incorrect classifications: " + (test_cases.size() - num_passed));

		} catch (NumberFormatException e) {
			System.out.println("Value for percent must be between 1 and 100");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error reading file");
			System.exit(0);
		}
	}
}
